﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    public GameObject player;

    public GameObject rightTransform;
    public GameObject leftTransform;

    public float speed = 1.0f;
    public float distanceToLose = 3f;

    public AudioSource pantingSound;
    public AudioSource growlSound;

    public List<AudioClip> growls = new List<AudioClip>();
    public List<AudioClip> eats = new List<AudioClip>();

    private GameController ctrl;
    void Start()
    {
        ctrl = FindObjectOfType<GameController>();
        StartCoroutine("MoveTowardsPlayer");
    }

    void Update()
    {
        Vector3 playerPos = player.transform.position;
        bool playerHidden = player.GetComponent<PlayerHide>().isHidden;

        if (!playerHidden && ctrl.gameRunning) 
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, playerPos, step);
            Quaternion rotation = Quaternion.LookRotation(transform.position - playerPos, Vector3.up);
            transform.rotation = rotation;
        }
    }

    float GetGrowlVolume( float playerMagnitude)
    {
        if (playerMagnitude < 15 && playerMagnitude > 10)
        {
            return .3f;

        }
        if (playerMagnitude < 10 && playerMagnitude > 5)
        {
             return .4f;

        }
        if (playerMagnitude < 5)
        {
            return .8f;
        }

        return 0.05f;
    }


    IEnumerator MoveTowardsPlayer()
    {
        while (ctrl.gameRunning)
        {
            Vector3 playerPos = player.transform.position;
            Vector3 playerDiff = transform.position - playerPos;
            float playerMagnitude = playerDiff.magnitude;
            float step = speed * Time.deltaTime;
            bool playerHidden = player.GetComponent<PlayerHide>().isHidden;

            if (Random.value > 0.95 && !playerHidden)
            {
                int random = Mathf.RoundToInt(Random.Range(0, growls.Count));
                AudioClip gruntToPlay = growls[random];
                growlSound.PlayOneShot(gruntToPlay);
            }

            if(playerHidden)
            {
                Vector3 playerCross = Vector3.Cross((playerPos - transform.position).normalized, Vector3.down);
                transform.position += playerCross;
                pantingSound.pitch = .8f;
            }
            else
            {
                pantingSound.pitch = 1f;
                
                growlSound.volume = GetGrowlVolume(playerMagnitude);
                pantingSound.pitch = GetGrowlVolume(playerMagnitude) + .9f;

            }

      

            if (Vector3.Distance(transform.position, playerPos) < distanceToLose)
            {
                Debug.Log("Hävisit");
                int random = Mathf.RoundToInt(Random.Range(0, eats.Count));
                Debug.Log("murina numero" + random);
                AudioClip gruntToPlay = eats[random];
                growlSound.PlayOneShot(gruntToPlay);
                pantingSound.Stop();
                FindObjectOfType<GameController>().OnGameOver();
                yield break;

            }

            yield return new WaitForSecondsRealtime(0.5f);
        }

        pantingSound.Stop();

    }
}
