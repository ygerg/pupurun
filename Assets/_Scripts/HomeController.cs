﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeController : MonoBehaviour
{

    public List<GameObject> homes = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        int random = Mathf.RoundToInt(Random.Range(0, homes.Count));
        homes[random].gameObject.tag = "Home";
        homes[random].gameObject.GetComponentInChildren<CapsuleCollider>().gameObject.tag = "Home";

    }
}
