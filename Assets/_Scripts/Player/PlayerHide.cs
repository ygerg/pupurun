﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PlayerHide : MonoBehaviour
{

    public PostProcessingProfile postProcess;
    public bool isHidden = false;

    private float hiddenExposure = -2f;
    private float intensityAmount = 0.385f;
    private float smoothnessAmount = 0.691f;


    public void Start() {
        ChangeExposure(0f, 0f, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hit trigger", other);
        if (other.tag == "Shelter")
        {
            ChangeExposure(hiddenExposure, intensityAmount, smoothnessAmount);
            isHidden = true;
        }

        if(other.tag == "Enemy")
        {
            ChangeExposure(0f, 0f, 0f);
        }

        if(other.tag == "Home")
        {
            FindObjectOfType<GameController>().OnGameWin();
        }
    }



    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Exit trigger", other);
        if (other.tag == "Shelter")
        {
            ChangeExposure(0f, 0f, 0f);
            isHidden = false;
        }
    }

    private  void ChangeExposure(float exposure, float intensity, float smoothness)
    {
        //copy current bloom settings from the profile into a temporary variable
        ColorGradingModel.Settings gradingSettings = postProcess.colorGrading.settings;
        VignetteModel.Settings vignetteSettings = postProcess.vignette.settings;

        //change the intensity in the temporary settings variable
        gradingSettings.basic.postExposure = exposure;
        vignetteSettings.intensity = intensity;
        vignetteSettings.smoothness = smoothness;

        //set the bloom settings in the actual profile to the temp settings with the changed value
        postProcess.colorGrading.settings = gradingSettings;
        postProcess.vignette.settings = vignetteSettings;
    }
}
