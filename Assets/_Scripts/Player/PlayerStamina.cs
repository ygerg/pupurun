﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStamina : MonoBehaviour
{
    public AudioClip fall;
    private AudioSource m_heartbeat;
    private float heartRate = 1f;
    public float stamina = 50f;
    private GameController m_gameCtrl;

    void Start()
    {
        m_gameCtrl = FindObjectOfType<GameController>();
        m_heartbeat = GetComponent<AudioSource>();
        StartCoroutine("PlayHeartbeats");
    }


    IEnumerator PlayHeartbeats()
    {
        while (m_gameCtrl.gameRunning)
        {
            float speed = FindObjectOfType<CharacterController>().velocity.magnitude;
            if (stamina > 50f) stamina = 50f;
            if (speed < 1f)
            {
                stamina += 3f;
            }
            else if (speed > 5f && stamina < 40f && stamina >= 20f)
            {
                stamina -= 2f;
            } else if (speed > 5f && stamina < 20f)
            {
                stamina -= 1f;
            }
            else
            {
                stamina -= 3f;
            }
            if (stamina >= 40f)
            {
                heartRate = 1f;
                m_heartbeat.Play();
            }
            else if (stamina < 40f && stamina >= 20f)
            {
                heartRate = 0.7f;
                m_heartbeat.Play();
            }
            else if (stamina < 20f) {
                heartRate = 0.4f;
                m_heartbeat.Play();
            }

            if (stamina <= 0f)
            {
                Debug.Log("Game over");
                m_heartbeat.PlayOneShot(fall, 0.7F);
                m_gameCtrl.OnRunToDeath();
                yield break;              
            }
            yield return new WaitForSecondsRealtime(heartRate);
        }
    }

}
