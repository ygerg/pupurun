﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScent : MonoBehaviour
{

    public GameObject scentTrail;


    private CharacterController m_playerController;
    private GameController m_gameCtrl;

    void Start()
    {
        m_playerController = gameObject.GetComponent<CharacterController>();
        m_gameCtrl = FindObjectOfType<GameController>();
        StartCoroutine("SpawnScent");
    }

    IEnumerator SpawnScent()
    {
        while(true)
        {
            if (Random.value > 0.2f & m_playerController.velocity.magnitude < 1f && m_gameCtrl.gameRunning)
            {
                Debug.Log("Spawning scent");
                Vector3 spawnPos = gameObject.transform.position + gameObject.transform.forward * 20f;
                Debug.Log(spawnPos);
                spawnPos.Set(spawnPos.x, spawnPos.y + 1f, spawnPos.z);
                Instantiate(scentTrail, spawnPos, Quaternion.identity);
            }
            yield return new WaitForSecondsRealtime(3f);

        }

    }

}
