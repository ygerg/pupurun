﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeScent : MonoBehaviour
{

    public float moveSpeed = 20f;
    private Vector3 m_homeTo;
    void Start()
    {
        m_homeTo = GameObject.FindWithTag("Home").transform.position;   
        StartCoroutine("MoveTowardsHome");
    }

    IEnumerator MoveTowardsHome()
    {
        yield return new WaitForSecondsRealtime(2f);
        float timer = 0f;
        while(timer < 100f)
        {
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, m_homeTo, step);
            ++timer;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2f);
        Destroy(gameObject);

    }
}
