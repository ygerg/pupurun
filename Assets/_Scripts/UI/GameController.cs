﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;


public class GameController : MonoBehaviour
{

    public bool gameRunning = true;
    public Animator panel1;
    public Animator panel2;

    public CanvasGroup gameOverButtons;
    public CanvasGroup gameWinButtons;

    public AudioMixer mixer;

    public Animator rabbitAnimator;
    public GameObject endHint1;
    public GameObject endHint2;

    void Start()
    {
        gameRunning = true;
        FindObjectOfType<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        StartCoroutine("RampUpAudio");
    }



    IEnumerator RampUpAudio()
    {
        float volume = -80f;
        while(volume < 1f)
        {
            mixer.SetFloat("MasterVolume", volume);
            volume += 0.3f;
            yield return new WaitForEndOfFrame();
        }

        SceneManager.UnloadSceneAsync(0);
    }


    public void StartOver()
    {
        SceneManager.LoadScene(0);
    }

    public void OnGameWin()
    {
        SetGameOverStuff();
        StartCoroutine("RunWinTasks");
    }

    IEnumerator RunWinTasks()
    {
        mixer.SetFloat("EnvVolume", 0f);
        while (gameWinButtons.alpha < 1f)
        {
            gameWinButtons.alpha += 0.01f;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.1f);

        while (true)
        {
            if (Input.anyKey)
            {
                SceneManager.LoadScene(0);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void SetGameOverStuff()
    {
        gameRunning = false;
        FindObjectOfType<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
        rabbitAnimator.SetBool("isRunning", false);
        rabbitAnimator.SetBool("isJumping", false);
    }

    public void OnGameOver()
    {
        endHint1.SetActive(false);
        endHint2.SetActive(true);
        SetGameOverStuff();
        StartCoroutine("RunGameOverTasks");

    }

    public void OnRunToDeath()
    {
        endHint1.SetActive(true);
        endHint2.SetActive(false);
        SetGameOverStuff();
        StartCoroutine("RunDiedOfExhaustionTasks");
    }

    IEnumerator RunDiedOfExhaustionTasks()
    {
        Quaternion targetRot = Quaternion.Euler(0f, 0f, -87f);

        // stupid
        float counter = 0f;
        while( counter < 200f)
        {
            Debug.Log(Camera.main.transform.localRotation.eulerAngles);
            Vector3 currPos = Camera.main.transform.position;
            //Camera.main.transform.position = new Vector3(currPos.x, Mathf.Clamp(currPos.y - 0.04f, 0.1f, .9f), currPos.z);
            Camera.main.transform.localRotation = Quaternion.Slerp(Camera.main.transform.localRotation, targetRot,
                   5f * Time.deltaTime);
            ++counter;
            yield return new WaitForEndOfFrame();
        }

        while (gameOverButtons.alpha < 1f)
        {

            gameOverButtons.alpha += 0.01f;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(0.1f);

        while (true)
        {
            if (Input.anyKey)
            {
                SceneManager.LoadScene(0);
            }
            yield return new WaitForEndOfFrame();
        }

    }


    IEnumerator RunGameOverTasks()
    {
        mixer.SetFloat("EnvVolume", 0f);
        panel1.SetBool("GameOver", true);
        panel2.SetBool("GameOver", true);
        yield return new WaitForSeconds(2f);
        while ( gameOverButtons.alpha < 1f)
        {
            gameOverButtons.alpha += 0.01f;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(0.1f);

        while (true)
        {
            if(Input.anyKey)
            {
                SceneManager.LoadScene(0);
            }
            yield return new WaitForEndOfFrame();
        }

    }
}
