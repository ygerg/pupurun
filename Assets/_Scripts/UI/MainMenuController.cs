﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{

    public CanvasGroup menuCGroup;

    private bool init = false;

    public void OnNewGame()
    {
        StartCoroutine("GameStartRoutine");
    }



    IEnumerator GameStartRoutine()
    {
        Debug.Log("ladataan");
        SceneManager.LoadScene(1, LoadSceneMode.Additive);

        yield return new WaitForSeconds(2f);

        while(menuCGroup.alpha > 0f)
        {
            menuCGroup.alpha -= 0.01f;
            yield return new WaitForEndOfFrame();
        }

    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            OnExit();
        }
        if(Input.anyKey && !init)
        {
            init = true;
            OnNewGame();
        }
    }

    public void OnExit()
    {

#if UNITY_EDITOR
        Debug.Log("In editor");
#else
        Application.Quit();
#endif

    }


}
